package World;
import Warriors.WarriorType;

public class WorldProperty {
	public static int intLifeElements;
	public static int NumerOfCity;
	public static int MaxMinutes;
	public static final int RED = 0;
	public static final int BLUE = 1;
	public static final String[] PartyNames = {"red headquarter", "blue headquarter"};
	public static final int[] RedProductionOrder={WarriorType.ICEMAN, WarriorType.LION, WarriorType.WOLF, WarriorType.NINJA, WarriorType.DRAGON};
	public static final int[] BlueProductionOrder={WarriorType.LION, WarriorType.DRAGON, WarriorType.NINJA, WarriorType.ICEMAN, WarriorType.WOLF};
	public static final int[] rewardNumber={0};
	
	public WorldProperty(){	}
}
