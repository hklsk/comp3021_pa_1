package World;

public class Clock {
	public int hour;
	public int minute;
	
	public Clock(){
		this.hour=0;
		this.minute=0;
	}
	public String customFormat(String a, int b){
		this.hour=b%60;
		
		if(this.hour<9)
		{
			a= "00"+this.hour;
		}
		else if(this.hour<99)
		{
			a= "0"+this.hour;
		}
		else
		{
			a =""+this.hour;
		}
		b%=60;
		if(b==0)
			return a+": 00";
		else
		a=""+minute;
		return a;
	}
	public String getTime(){
		return customFormat("",getMinute());
	}
	public int getHour() {
		return hour;
	}
	public int getMinute(){
		return minute;
	}
	public void increase(){
		this.minute+=10;
		if(this.minute > 60) {
			this.minute = this.minute - 60;
			this.hour++;
		}
	}
	public int getTotalMinutes() {
		return this.hour * 60 + this.minute;
	}
}
