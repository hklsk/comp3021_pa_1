package World;
import java.util.ArrayList;

import Warriors.Warrior;

public class World extends City{
	public static final int MAX_LIFE = 1000;
	public static final int MAX_CITY = 1000;
	public static final int MAX_TIME = 1000;
	public static final int MIN_CITY = 1;
	private int endTime;
	private boolean gameOver = false;
	private ArrayList<City> cityList;
	private ArrayList<Warrior> warriorList;
	private Clock clock;
	public World(int numOfLife, int numOfCity, int endTime){
		numOfCity = Math.min(Math.max(MAX_CITY, numOfCity), MIN_CITY);
		numOfLife = numOfLife; // check error
		this.endTime = endTime;
		this.clock = new Clock();
		this.cityList = new ArrayList<City>();
		this.cityList.add(new Headquarters());
		for(int i=0; i<numOfCity; i++) {
			this.cityList.add(new City());
		}
		this.cityList.add(new Headquarters());
	}
	public void runGame(){
		gameOver = false;
		while(true) {
			
			if(clock.getTotalMinutes() < this.endTime) {
				break;
			}
			if(clock.getMinute() == 0) {
				this.cityList.get(0).addWarrior(new Warrior());
				this.cityList.get(this.cityList.size()-1).addWarrior(new Warrior());
			} else if(clock.getMinute() == 10) {
				for(int i=0; i<this.cityList.size(); i++) {
					for(int j=0; j<this.cityList.get(i).RedWarriorStation.size(); j++) {
						
					}
					for(int j=0; j<this.cityList.get(i).BlueWarriorStation.size(); j++) {
						
					}
				}
			} else if(clock.getMinute() == 20) {
				for(City city : this.cityList) {
					city.LifeElement+=10;
				}
			} else if(clock.getMinute() == 30) {
				// warrior get element
			} else if(clock.getMinute() == 40) {
				// warrior get element
				for(City city : this.cityList) {
					for(Warrior redWarrior : city.RedWarriorStation) {
						
					}	
				}
			} else if(clock.getMinute() == 50) {
				// Report
			}
			clock.increase();
		}
	}
	public void holdBattlesAndWorkAfterBatthels(){
		
	}
	public void headquartersReportLifeElements(){
		
	}
	public void warriorsFetchLifeElementsFromCity(){
		
	}
	public void ProduceLifeElements(){
		
	}
	public void marchWarriors(){
		
	}
	public void warriorReportMarch(City a, Warrior b){
		
	}
}
