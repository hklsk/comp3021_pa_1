package World;

import java.util.ArrayList;
import Warriors.Warrior;

public class City {
	public int CityID;
	public int LifeElement;
	public ArrayList<Warrior> RedWarriorStation;
	public ArrayList<Warrior> BlueWarriorStation;
	public int Flag;
	public int PartyOfLastRoundWinner;
	public boolean Status_AfterBattle;
	
	public City(){}
	public City(int a){
		this.CityID=a;
		this.LifeElement=0;
		this.RedWarriorStation=new ArrayList<Warrior>();
		this.BlueWarriorStation=new ArrayList<Warrior>();
		this.Flag=-1;
		this.PartyOfLastRoundWinner=-1;
		this.Status_AfterBattle=false;
	}
	public void produceLifeElement(){
		this.LifeElement+=10;
	}
	public int popLifeElement(){
		return this.LifeElement;
	}
	public void addWarrior(Warrior a){
		if(a.Type==0)
			this.RedWarriorStation.add(a);
		else
			this.BlueWarriorStation.add(a);
	}
	public void removeWarrior(Warrior a){
		if(a.Type==0)
			this.RedWarriorStation.remove(a);
		else
			this.BlueWarriorStation.remove(a);
	}
	public boolean organizeBattle(){
		//????
		return false;
	}
	public void payTribute(){
		this.LifeElement=0;
	}
}
