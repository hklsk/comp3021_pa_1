package Game;
import World.World;
import java.util.Scanner;

import Warriors.WarriorType;

public class Main {
	public static void main (String ...strings) {
		int [] num;
		Scanner scanner = new Scanner(System.in);
		num = new int[3];
		for(int i=0; i<3; i++) {
			num[i] = scanner.nextInt();
		}
		World world = new World(num[1], num[2], num[3]);
		WarriorType.hp_list = new int[5];
		for(int i=0; i<5; i++) {
			int numInput = scanner.nextInt(); //check Input
			WarriorType.hp_list[i] = numInput;
		}
		WarriorType.attack_list = new int[5];
		for(int i=0; i<5; i++) {
			int numInput = scanner.nextInt(); //check Input
			WarriorType.attack_list[i] = numInput;
		}
		world.runGame();
	}
}
